(ns userservice.routes
  (:require [org.httpkit.server :as httpkit]
            [ring.util.http-response :as resp]
            [cheshire.core :refer :all]
            [userservice.mongodb.mongo :as mongo]
            [userservice.authorization :as auth]))

;(defn serve-index [_]
;  (-> "<html><h1>Hello</h1></html>"
;      (resp/ok)
;      (resp/content-type "text/html; charset=UTF-8")))

(defn serve-ping [_]
  (-> "pong"
      (resp/ok)
      (resp/content-type "text/html; charset=UTF-8")))

(defn serve-profile [{:keys [params] :as request}]
  (prn "Serving profile for request: " request)
  (let [user (:username params)
        session-token (:session-token params)
        profile (if (auth/authorized? user session-token)
                  (mongo/get-profile user)
                  (resp/unauthorized!))]
    (if profile
      (-> profile
          (resp/ok)
          (resp/content-type "text/html; charset=UTF-8"))
      (resp/not-found))))

(def routes ["/" {["ping/" :name] {:get serve-ping}
                  ["profile"]     {:get serve-profile}}])


