(ns userservice.mongodb.mongo
  (:require [monger.core :as monger]
            [monger.collection :as mc]))

(def db-name "user")

(def collection "user")

(defn get-profile [user]
  (let [connection   (monger/connect)
        db           (monger/get-db connection db-name)
        user-profile (mc/find-maps db collection {:user user})]
    user-profile))

